//
//  AlbumListCustomTableViewCell.swift
//  AlbumPOC
//
//  Created by Ashlesha Tripathy on 10/09/21.
//

import UIKit
class AlbumListCustomTableViewCell: UITableViewCell {
    @IBOutlet weak var nameField: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
