//
//  AlbumListViewModel.swift
//  AlbumPOC
//
//  Created by Ashlesha Tripathy on 09/09/21.
//

import Foundation

typealias AlbumDataDBResponse = (_ albumList: [Album]?, _ error: Error?) -> Void

class AlbumListViewModel {
    let apiService = ApiClientService()
    var onFetchingAlbums:((_ result: [Album]?)->Void)?

    func fetchAlbums() {
        self.apiService.getResponseModel(request: ApiRequest.fetchAlbum) { (items, error) in
            if error == nil {
                if let result = items {
                    self.saveAlbumInfoToDB(albums: result)
                    self.onFetchingAlbums?(result)
                    return
                }else {
                    self.onFetchingAlbums?(nil)
                    return
                }
            }else {
                self.onFetchingAlbums?(nil)
                return
            }
        }
    }
    
    func saveAlbumInfoToDB(albums: [Album]) {
        CoreDataHelper.shared.deleteAllData(entity: Constant.DBEntities.album) { (_, _) in
        }
        
        for info in albums {
            let value = [AlbumInfoDBValues.userId.rawValue: info.userId ?? 0,
                         AlbumInfoDBValues.id.rawValue: info.id ?? 0,
                         AlbumInfoDBValues.title.rawValue: info.title ?? ""
            ] as [String : Any]
            
            CoreDataHelper.shared.saveData(entity: Constant.DBEntities.album, data: value as [String : Any]) { (_, _) in
            }
        }
    }
    func getAlbumsFromDB(completion: @escaping (AlbumDataDBResponse)) -> Void {
        var albumList = [Album]()
        var dbResponse = [AlbumInfo]()
        CoreDataHelper.shared.retrieveData(entityName: Constant.DBEntities.album, predicate: nil) { (response, error) in
             if let result = response as? [AlbumInfo] {
                 dbResponse = result
             }
             for info in dbResponse {
                let album = Album(userId: Int(info.userId), id: Int(info.id), title: info.title)
                albumList.append(album)
             }
            completion(albumList, error)
         }
    }
}


