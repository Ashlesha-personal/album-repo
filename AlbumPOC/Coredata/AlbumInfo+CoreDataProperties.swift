//
//  AlbumInfo+CoreDataProperties.swift
//  AlbumPOC
//
//  Created by Ashlesha Tripathy on 10/09/21.
//
//

import Foundation
import CoreData


extension AlbumInfo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AlbumInfo> {
        return NSFetchRequest<AlbumInfo>(entityName: "AlbumInfo")
    }

    @NSManaged public var userId: Int32
    @NSManaged public var id: Int32
    @NSManaged public var title: String?

}

extension AlbumInfo : Identifiable {

}
