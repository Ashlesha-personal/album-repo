//
//  CoreDataHelper.swift
//  AlbumPOC
//
//  Created by Ashlesha Tripathy on 10/09/21.
//

import Foundation
import CoreData

typealias RetriveDataResponse = (_ resultObject: [NSManagedObject]?, _ error: Error?) -> Void
typealias CoreDataResponse = (_ success: Bool, _ error: Error?) -> Void

class CoreDataHelper {
    
    static let shared = CoreDataHelper()
    
    func retrieveData(entityName: String, predicate: NSPredicate?, fetchCompletion: @escaping (RetriveDataResponse)) -> Void {
        let managedContext = CoreDataStack.shared.context
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.returnsObjectsAsFaults = false
        
        do {
            let result = try managedContext.fetch(fetchRequest)
            if let response = result as? [NSManagedObject] {
                
                fetchCompletion(response, nil)
            }
        }
        catch let error {
            fetchCompletion(nil, error)
        }
    }
    
    func saveData(entity: String, data: [String: Any], completion: @escaping(CoreDataResponse)) -> Void {
        let managedContext = CoreDataStack.shared.context
        CoreDataStack.shared.context.perform {
            if let entity = NSEntityDescription.entity(forEntityName: entity, in: managedContext) {
                let managedObj = NSManagedObject(entity: entity, insertInto: managedContext)
                for (key, value) in data {
                    
                    if value is NSNull {
                    }
                    else {
                        managedObj.setValue(value, forKey: key)
                    }
                }
                
                
                do {
                    try managedContext.save()
                    completion(true, nil)
                } catch let error {
                    completion(false, error)
                }
            }
        }
    }

    func deleteAllData(entity: String, deleteAllWithoutPredicate: Bool = false, deleteCompletion: @escaping (CoreDataResponse)) -> Void {
        
        let managedContext = CoreDataStack.shared.context
        CoreDataStack.shared.context.perform {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
            fetchRequest.returnsObjectsAsFaults = false
            
            do {
                let results = try managedContext.fetch(fetchRequest)
                for managedObject in results {
                    let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                    managedContext.delete(managedObjectData)
                }
                deleteCompletion(true, nil)
            } catch let error {
                deleteCompletion(false, error)
            }
        }
    }
    
}

