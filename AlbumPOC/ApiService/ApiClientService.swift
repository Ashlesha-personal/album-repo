//
//  ApiClientService.swift
//  AlbumPOC
//
//  Created by Ashlesha Tripathy on 09/09/21.
//

import Foundation

public protocol RequestGetter {
    func urlRequest(urlString: String) -> URLRequest?
}

typealias responseHandler = (Data?, Error?) -> Void


// API class
class ApiClientService:NSObject {
    override init() {
    }
}

// Enum
enum ApiRequest: RequestGetter {
    case fetchAlbum
    
    var method:String {
        switch self {
        case .fetchAlbum:
            return "GET"
        }
    }
    var baseUrl:String {
        switch self {
        case .fetchAlbum:
            return Constant.UrlConstant.baseUrl
        }
    }
    var path:String {
        switch self {
        case .fetchAlbum:
            return Constant.UrlConstant.getAlbumUrlPath
        }
    }
    
    var body:Data? {
        switch self {
        case .fetchAlbum:
            return nil
        }
    }
    
    var headers:[String:String] {
        switch self {
        case .fetchAlbum:
            return ["Content-Type": "application/json"]
        }
    }
    
    func urlRequest(urlString: String) -> URLRequest? {
        guard let url = URL(string: self.baseUrl + self.path) else { return nil }
        var request = URLRequest(url: url)
        request.allHTTPHeaderFields = headers
        request.httpMethod = method
        if let requestBody = self.body {
            request.httpBody = requestBody
        }
        
        return request
    }
}


extension ApiClientService {
    func getResponseModel(request:RequestGetter, onCompletion: @escaping ([Album]?,Error?) -> Void) {
        
        self.getResponseData(with: request) { (data, error) in
            
            var albums = [Album]()
            
            if error == nil {
                do{
                    if let responseData = data {
                        let decoder = JSONDecoder()
                        albums = try decoder.decode([Album].self, from: responseData)
                        onCompletion(albums, nil)
                    }else {
                        onCompletion(nil, error)
                    }
                    
                } catch _ {
                    onCompletion(nil, error)
                }
            }else {
                //error
                onCompletion(nil, error)
            }
        }
    }
    
    func getResponseData(with contantUrlRequest:RequestGetter ,onCompletion: @escaping responseHandler) {
        if let myUrlRequest =  contantUrlRequest.urlRequest(urlString: "") {
            
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig)
            
            let dataTask = session.dataTask(with: myUrlRequest  , completionHandler: { (data, response, error) -> Void in
                
                if error == nil {
                    if let dataResponse = data, let httpResponse = response as? HTTPURLResponse {
                        if httpResponse.statusCode == 204 || httpResponse.statusCode == 200 {
                            onCompletion(dataResponse, nil)
                            return
                        }else {
                            let errorMessage = Constant.ErrorConstant.apiErrorMessage
                           
                            let userInfo: [String : Any] =
                                [NSLocalizedDescriptionKey : NSLocalizedString("", value: errorMessage , comment: "")]
                            let errorObj = NSError(domain: "", code: 0, userInfo: userInfo)
                            onCompletion(nil, errorObj)
                            return
                        }
                    }
                }else {
                    onCompletion(nil, error)
                    return
                }
            })
            dataTask.resume()
        }
    }
}
