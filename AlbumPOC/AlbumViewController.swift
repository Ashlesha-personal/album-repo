//
//  AlbumViewController.swift
//  AlbumPOC
//
//  Created by Ashlesha Tripathy on 09/09/21.
//

import UIKit

class AlbumViewController: BaseViewController {
    
    let albumViewModel = AlbumListViewModel()
    var albumDatasource = [Album]()
    @IBOutlet weak var albumListView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.albumListView.delegate = self
        self.albumListView.dataSource = self
        
        self.albumViewModel.fetchAlbums()
        self.albumViewModel.onFetchingAlbums = { (albums) in
            if self.albumDatasource.count > 0 {
                self.albumDatasource.removeAll()
            }
            
            if let result = albums {
                self.albumDatasource = result
                
            }else {
                //fetch offline data
                self.albumViewModel.getAlbumsFromDB { (response, error) in
                    if let result = response, result.count > 0 {
                        self.albumDatasource = result
                    }
                }
                
                DispatchQueue.main.async {
                    self.albumListView.reloadData()
                }
            }
        }
    }
    
}

extension AlbumViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.albumDatasource.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.albumListView.dequeueReusableCell(withIdentifier: Constant.Identifiers.albumListCustomCell) as! AlbumListCustomTableViewCell
        
        if self.albumDatasource.count > 0 {
            let sortedDatasource = self.albumDatasource.sorted(by: { $0.title ?? "" < $1.title ?? "" })
            cell.nameField.text = sortedDatasource[indexPath.row].title ?? ""
        }
        return cell
    }
}

