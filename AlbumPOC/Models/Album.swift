//
//  Album.swift
//  AlbumPOC
//
//  Created by Ashlesha Tripathy on 09/09/21.
//

import Foundation

struct Album: Codable {
    let userId: Int?
    let id: Int?
    let title: String?
    
    enum CodingKeys: String, CodingKey {
        case userId, title, id
    }
}
