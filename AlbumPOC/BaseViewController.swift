//
//  BaseViewController.swift
//  AlbumPOC
//
//  Created by Ashlesha Tripathy on 09/09/21.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
  
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
}
