//
//  Constant.swift
//  AlbumPOC
//
//  Created by Ashlesha Tripathy on 09/09/21.
//

import Foundation

class Constant: NSObject {
    
    struct UrlConstant {
      static let baseUrl = "https://jsonplaceholder.typicode.com"
      static let getAlbumUrlPath = "/albums"
    }
    
    struct ErrorConstant {
        static let apiErrorMessage = NSLocalizedString("Something went wrong", comment: "")
    }
    
    struct Identifiers {
        static let albumListCustomCell = "AlbumListCustomCellId"
    }
    
    struct DBEntities {
        static let album = "AlbumInfo"
    }
    
}

public enum AlbumInfoDBValues: String {
    case userId = "userId"
    case id = "id"
    case title = "title"
}
